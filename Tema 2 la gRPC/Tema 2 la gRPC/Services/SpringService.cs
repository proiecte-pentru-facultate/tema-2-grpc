﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Tema_2_la_gRPC.Utils_objects;
using System.IO;

namespace Tema_2_la_gRPC.Services
{
    public class SpringService : Spring.SpringBase
    {
        private readonly ILogger<SpringService> _logger;
        SpringService(ILogger<SpringService> logger)
        {
            _logger = logger;
        }

        public override Task<AstralSign> GetZodiaSignByDate(CalendarDate request, ServerCallContext context)
        {

            var list = JsonConvert.DeserializeObject<List<IntervalForSigncs>>(File.ReadAllText("Spring.json"));

            AstralSign sign = null;

            foreach (var obj in list)
            {
                if (request.Month == obj.BeginMonth && (Int32.Parse(request.Day) >= obj.BeginDay))
                {

                    sign.NameOfTheSign = obj.ZodiacSing;

                    return Task<AstralSign>.FromResult(sign);
                }
                else if (request.Month == obj.EndMonth && (Int32.Parse(request.Day) <= obj.EndDay))
                {
                    sign.NameOfTheSign = obj.ZodiacSing;

                    return Task<AstralSign>.FromResult(sign);
                }
            }

            return null;
        }

    }
}
