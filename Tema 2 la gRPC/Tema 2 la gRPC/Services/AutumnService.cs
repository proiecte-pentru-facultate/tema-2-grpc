﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Json.Net;
using Tema_2_la_gRPC.Utils_objects;
using Newtonsoft.Json;
using System.IO;

namespace Tema_2_la_gRPC.Services
{
    public class AutumnService : Autumn.AutumnBase
    {
        private readonly ILogger<AutumnService> _logger;
        AutumnService(ILogger<AutumnService> logger)
        {
            _logger = logger;
        }

        public override Task<AstralSign> GetZodiaSignByDate(CalendarDate request, ServerCallContext context)
        {

            var list = JsonConvert.DeserializeObject<List<IntervalForSigncs>>(File.ReadAllText("Autumn.json"));

            AstralSign sign = null;

            foreach (var obj in list)
            {
                if (request.Month == obj.BeginMonth && (Int32.Parse(request.Day) >= obj.BeginDay))
                {

                    sign.NameOfTheSign = obj.ZodiacSing;

                    return Task<AstralSign>.FromResult(sign);
                }
                else if (request.Month == obj.EndMonth && (Int32.Parse(request.Day) <= obj.EndDay))
                {
                    sign.NameOfTheSign = obj.ZodiacSing;

                    return Task<AstralSign>.FromResult(sign);
                }
            }

            return null;

        }

    }
}
