﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using Grpc.Net.Client;




namespace Tema_2_la_gRPC.Services
{
    public class ZodiacSignService : ZodiacSign.ZodiacSignBase
    {
        private readonly ILogger<ZodiacSignService> _logger;

        public ZodiacSignService(ILogger<ZodiacSignService> logger)
        {
            _logger = logger;
        }

        public override Task<AstralSign> GetCalendarDate(CalendarDate request, ServerCallContext context)
        {

            var season = GetSeasonByDate(request);

            var channel = GrpcChannel.ForAddress("https://localhost:5001");

            AstralSign sign = null;

            if (season == "Spring")
            {
                var client = new Spring.SpringClient(channel);

                sign = client.GetZodiaSignByDate(request);
            }
            else if (season == "Summer")
            {
                var client = new Summer.SummerClient(channel);

                sign = client.GetZodiaSignByDate(request);
            }
            else if (season == "Autumn")
            {
                var client = new Autumn.AutumnClient(channel);

                sign = client.GetZodiaSignByDate(request);
            }
            else
            {
                var client = new Winter.WinterClient(channel);

                sign = client.GetZodiaSignByDate(request);
            }

            return Task.FromResult(sign);
        }

        String GetSeasonByDate(CalendarDate date)
        {
            if ((date.Month == "March") || (date.Month == "April") || (date.Month == "May"))
                return "Spring";
            else if ((date.Month == "June") || (date.Month == "July") || (date.Month == "August"))
                return "Summer";
            else if ((date.Month == "September") || (date.Month == "October") || (date.Month == "November"))
                return "Autumn";
            else return "Winter";
        }

    }
}
